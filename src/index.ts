import fs from 'fs';
import Bean from './adapter/Bean.js';
import EntryObject from './adapter/EntryObject.js';

let bean = await Bean();

let argvs = process.argv.slice(2);

let type = argvs[0];

const file = argvs[1];

switch (type) {

    case "export" : {

        bean.export.retrieveAll()
            .then
            (
                output => fs.writeFileSync
                (
                    `${file}/export-${Date.now()}.json`,

                    JSON.stringify(output, null, 2),
                    {
                        encoding: 'utf8',
                        flag: 'w'
                    }
                )
            )
            .finally(() => console.log(`Exportanto arquivo para pasta: '${file}'`));

        break;

    }

    case "import" : {

        console.log(`Importanto arquivo ${file}`);

        bean.import
            .usersGroups
            (
                EntryObject
                (
                    JSON.parse
                    (
                        fs.readFileSync
                        (
                            file,
                            {encoding: 'utf8', flag: 'r'}
                        )
                    )
                )
            ).then(() => console.log("Importação concluída"));

        break;
    }

    default : {

        console.log("\nKeycloak Admin CLI : Cadastrado e exportação de usuários com seus respectivos grupos\n")

        console.log("Para importar execute : 'npm start -type import -f input.json'\n")

        console.log("Para exportar execute : 'npm start -type export -output pasta'\n")

        console.log("\n" +
            "| Argumento | Descrição          |\n" +
            "|-----------|--------------------|\n" +
            "| -type     | Import ou Export   |\n" +
            "| -file     | Arquivo de entrada |\n" +
            "| -output   | Pasta de saida     |\n\n")

    }
}
