import {UserGateway} from "./Gateway";

export class User {

    constructor(private name: string, email?: string, password?: string) {
        this.email = email;
        this.password = password || "odin";
    }

    private readonly email?: string;

    private readonly password: string;

    public async register(gateway: UserGateway, groupName: string, groupId: string): Promise<void> {

        if (await gateway.existByName(this.name)) {

            if (!await gateway.belongsToTheGroup(this.name, groupName)) await gateway.registerInGroup(this.name, groupName, groupId);

        } else if (!await gateway.existByEmail(this.email)) {

            await gateway.register(this.name, this.email, this.password, groupName);

        } else throw new Error(`Já existe na base de dados um usuário com email ${this.email}`);

    }

    public toType(): UserType {
        return {
            name: this.name,
            email: this.email,
            password: this.password
        }
    }

}

export interface UserType {

    name: string;

    email: string | undefined;

    password: string;

}