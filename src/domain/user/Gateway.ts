export interface UserGateway {

    existByName(name: string): Promise<boolean>;

    register(name: string, email: string | undefined, password: string, group: string): Promise<void>;

    registerInGroup(name: string, groupName: string, groupId: string): Promise<void>;

    belongsToTheGroup(name: string, group: string): Promise<boolean>;

    existByEmail(email: string | undefined): Promise<boolean>;

}