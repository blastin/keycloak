import {UserGateway} from "./Gateway";

export class UserGatewayLogs implements UserGateway {

    constructor(private readonly gateway: UserGateway) {
    }

    belongsToTheGroup(name: string, group: string): Promise<boolean> {
        console.log(`Verificando se o usuário '${name}' já foi cadastrado no grupo '${group}' `);
        return this.gateway.belongsToTheGroup(name, group);
    }

    existByEmail(email: string | undefined): Promise<boolean> {
        console.log(`Verificando se existe um usuario com email '${email}' cadastrado`);
        return this.gateway.existByEmail(email);
    }

    existByName(name: string): Promise<boolean> {
        console.log(`Verificando se o usuário '${name}' já foi cadastrado`);
        return this.gateway.existByName(name);
    }

    register(name: string, email: string | undefined, password: string, group: string): Promise<void> {
        console.log(`Registrando Usuário '${name}'`);
        return this.gateway.register(name, email, password, group);
    }

    registerInGroup(name: string, groupName: string, groupId: string): Promise<void> {
        console.log(`Registrando Usuário '${name}' em grupo '${groupName}`);
        return this.gateway.registerInGroup(name, groupName, groupId);
    }

}