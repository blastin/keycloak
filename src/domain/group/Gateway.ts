import {Group} from "./Group";

export interface GroupGateway {

    exist(name: string): Promise<boolean>;

    register(name: string): Promise<void>;

    getID(name: string): Promise<string>;

    all(): Promise<Array<Group>>;
}