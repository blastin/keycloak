import {User, UserType} from "../user/User";
import {UserGateway} from "../user/Gateway";
import {GroupGateway} from "./Gateway";

export class Group {

    constructor(private name: string, private users: Array<User>) {
    }

    public async register(groupGateway: GroupGateway, userGateway: UserGateway): Promise<void> {

        if (!await groupGateway.exist(this.name)) await groupGateway.register(this.name);

        for (const user of this.users) {
            await user.register(userGateway, this.name, await groupGateway.getID(this.name));
        }

    }

    public toType(): GroupType {
        return {
            name: this.name,
            users: this.users.map(user => user.toType())
        }
    }

}

export interface GroupType {

    name: string;

    users: Array<UserType>;

}
