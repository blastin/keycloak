import {GroupGateway} from "./Gateway";
import {Group} from "./Group";

export class GroupGatewayLog implements GroupGateway {

    constructor(private readonly gateway: GroupGateway) {
    }

    all(): Promise<Array<Group>> {
        console.log("Recuperando todos usuarios");
        return this.gateway.all();
    }

    exist(name: string): Promise<boolean> {
        console.log(`Pesquisando em keycloak se o grupo '${name}' já foi cadastrado`);
        return this.gateway.exist(name);
    }

    getID(name: string): Promise<string> {
        console.log(`Pesquisando em keycloak id do grupo '${name}'`);
        return this.gateway.getID(name);
    }

    register(name: string): Promise<void> {
        console.log(`Registrando Grupo '${name}'`);
        return this.gateway.register(name);
    }

}