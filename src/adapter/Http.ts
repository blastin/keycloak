export interface Http {

    get(url: string): Promise<any>;

    post(url: string, body: any): Promise<void>;

    put(url: string, body?: any): Promise<void>;

}

