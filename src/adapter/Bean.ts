import {Adapter as GroupLibraryAdapter} from './group/library/Adapter.js';
import {Adapter as UserLibraryAdapter} from './user/library/Adapter.js';
import {Import} from '../application/Import.js';
import {Export} from "../application/Export.js";
import {Client as GroupClientAdapter} from "./group/client/Client.js";
import {Client as UserClientAdapter} from "./user/client/Client.js";
import KcAdminClient from '@keycloak/keycloak-admin-client';
import * as dotenv from 'dotenv';
import {HttpInterceptorProxy} from "./HttpInterceptorProxy.js";
import {GroupGatewayLog} from "../domain/group/GroupGatewayLog.js";
import {UserGatewayLogs} from "../domain/user/UserGatewayLogs.js";
import {Http} from "./Http";
import {GroupGateway} from "../domain/group/Gateway";

import agent from 'http-proxy-agent';

export interface Bean {

    import: Import;
    export: Export;

}

export default async function (): Promise<Bean> {

    dotenv.config();

    function getHttpInterceptorProxy() {

        return new HttpInterceptorProxy
        (`${process.env.BASE_URL_KEYCLOAK}/${process.env.TOKEN_URL_KEYCLOAK}`,

            {
                'client_id': `${process.env.CLIENT_ID_KEYCLOAK}`,
                'client_secret': `${process.env.CLIENT_SECRET_KEYCLOAK}`,
                'scope': 'openid',
                'grant_type': 'client_credentials'
            },
            new agent.HttpProxyAgent({
                host: `${process.env.PROXY_HOST}`,
                enableTrace: true,
                port: `${process.env.PROXY_PORT}`,
                protocol: "http"
            })
        );
    }

    function getGroupClientAdapter(httpInterceptorProxy: Http) {

        return new GroupGatewayLog
        (
            new GroupClientAdapter
            (
                `${process.env.BASE_URL_KEYCLOAK}/admin/realms/${process.env.REALM_NAME_KEYCLOAK}`,
                httpInterceptorProxy
            )
        );

    }

    function getUserClientAdapter(httpInterceptorProxy: Http, groupClientAdapter: GroupGateway) {

        return new UserGatewayLogs
        (
            new UserClientAdapter
            (
                `${process.env.BASE_URL_KEYCLOAK}/admin/realms/${process.env.REALM_NAME_KEYCLOAK}`,
                httpInterceptorProxy,
                groupClientAdapter
            )
        );

    }

    async function getKcAdminClient() {

        const kcAdminClient = new KcAdminClient({
            baseUrl: `${process.env.BASE_URL_KEYCLOAK}`,
            realmName: `${process.env.REALM_NAME_KEYCLOAK}`
        });

        await kcAdminClient.auth({
            clientId: "admin-cli",
            username: `${process.env.USERNAME_KEYCLOAK}`,
            password: `${process.env.PASSWORD_KEYCLOAK}`,
            grantType: "password"
        });


        return kcAdminClient;
    }

    try {

        const kcAdminClient = await getKcAdminClient();

        const groupGateway: GroupGateway = new GroupGatewayLog(new GroupLibraryAdapter(kcAdminClient));

        return {
            import: new Import
            (
                new UserGatewayLogs(new UserLibraryAdapter(kcAdminClient)),
                groupGateway
            ),
            export: new Export(groupGateway)
        };

    } catch (e) {

        console.error(e);

        const httpInterceptorProxy = getHttpInterceptorProxy();

        const groupClientAdapter = getGroupClientAdapter(httpInterceptorProxy);

        return {

            export: new Export(groupClientAdapter),
            import: new Import
            (
                getUserClientAdapter(httpInterceptorProxy, groupClientAdapter),
                groupClientAdapter
            )

        }

    }

}