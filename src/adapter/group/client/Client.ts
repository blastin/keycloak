import {GroupGateway} from "../../../domain/group/Gateway";
import {Group} from "../../../domain/group/Group.js";
import {Http} from "../../Http";
import {User} from "../../../domain/user/User.js";
import {GroupResponse} from "../GroupResponse";
import {UserResponse} from "../../user/UserResponse";

export class Client implements GroupGateway {
    constructor(baseURL: string, http: Http) {
        this.baseURL = baseURL;
        this.http = http;
    }

    private readonly baseURL: string;

    private readonly http: Http;

    async all(): Promise<Array<Group>> {

        const groups : Array<GroupResponse> = await this.http.get(`${this.baseURL}/groups`)

        const array: Array<Group> = new Array<Group>();

        for (const group of groups) {

            const users: Array<UserResponse> = await this.http.get(`${this.baseURL}/groups/${group.id}/members`)

            array.push(new Group(group.name, users.map(user => new User(user.username, user.email))));

        }

        return array;

    }

    async exist(name: string): Promise<boolean> {

        const json: Array<GroupResponse> = await this.http.get(`${this.baseURL}/groups?search=${name}`)

        return json.length > 0;

    }

    async getID(name: string): Promise<string> {

        const json: Array<GroupResponse> = await this.http.get(`${this.baseURL}/groups?search=${name}`)

        let group: GroupResponse | undefined = json.pop();

        if (group === undefined) {
            throw new Error(`Não foi possível encontrar ID do grupo ${name}`);
        }

        return group.id;

    }

    async register(name: string): Promise<void> {

        await this.http.post
        (
            `${this.baseURL}/groups`,
            {
                "name": name
            }
        )

    }

}