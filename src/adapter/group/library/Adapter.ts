import {GroupGateway} from "../../../domain/group/Gateway";

import KcAdminClient from '@keycloak/keycloak-admin-client';
import {Group} from "../../../domain/group/Group.js";
import {User} from "../../../domain/user/User.js";
import UserRepresentation from "@keycloak/keycloak-admin-client/lib/defs/userRepresentation";
import {Output} from "../Output";

export class Adapter implements GroupGateway {

    private cache: Map<string, string>;

    constructor(private admin: KcAdminClient) {
        this.cache = new Map<string, string>();
    }

    async groups(user: UserRepresentation): Promise<Output[]> {

        function create(user: UserRepresentation, group?: string): Output {

            return {

                user: new User
                (
                    user.username || "",
                    user.email,
                    user.credentials?.find(predicate => predicate.type == "password")?.value
                ),
                group: group || ""

            };
        }

        const groups = await this.admin.users.listGroups({id: user.id || ""});

        return groups.map(value => create(user, value.name));

    }

    async all(): Promise<Array<Group>> {

        const users = await this.admin.users.find();

        const objects = users.map
        (
            async user => await this.groups(user)
        );

        const map = new Map<string, User[]>();

        for (const value of objects) for (const output of await value) {

            const possible = map.get(output.group);

            if (possible != undefined) possible.push(output.user);
            else map.set(output.group, [output.user]);

        }

        const groups: Array<Group> = new Array<Group>();

        for (const [key, value] of map) {
            groups.push(new Group(key, value));
        }

        console.log("Retornando usuários com seus respectivos grupos");

        return groups;

    }

    async exist(name: string): Promise<boolean> {

        if (this.cache.get(name) != undefined) {
            console.log(`Grupo '${name}' foi encontrado. Recuperando em cache`);
            return true;
        }

        const promise = await this.admin.groups.find({search: name});

        const group = promise.pop();

        if (group != undefined) {

            this.cache.set(name, group.id || "");

            console.log(`Grupo '${name}' foi encontrado. Guardado em cache`);

        } else console.log(`Grupo '${name}' não foi encontrado`);

        return group != undefined;

    }

    async register(name: string): Promise<void> {

        const promise = await this.admin.groups.create({
            name: name
        });

        console.log(`Guardando Grupo '${name}:${promise.id}' em cache`);

        this.cache.set(name, promise.id);

    }

    async getID(name: string): Promise<string> {

        const id = this.cache.get(name);

        if (id == undefined) throw new Error(`Grupo '${name}' não cadastrado`);

        return id;

    }

}