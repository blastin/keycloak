import {User} from "../../domain/user/User";

export interface Output {

    user: User;
    group: string;

}