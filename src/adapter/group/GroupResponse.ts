export interface GroupResponse {
    name: string;
    id: string;
}