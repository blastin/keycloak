import {UserGateway} from "../../../domain/user/Gateway";
import {Http} from "../../Http";
import {GroupGateway} from "../../../domain/group/Gateway";
import {UserResponse} from "../UserResponse";

export class Client implements UserGateway {
    constructor(baseURL: string, http: Http, groupGateway: GroupGateway) {
        this.baseURL = baseURL;
        this.http = http;
        this.groupGateway = groupGateway;
    }

    private readonly baseURL: string;

    private readonly http: Http;

    private readonly groupGateway: GroupGateway;

    async belongsToTheGroup(name: string, group: string): Promise<boolean> {

        let id = await this.groupGateway.getID(group);

        const response: Array<UserResponse> = await this.http.get(`${this.baseURL}/groups/${id}/members?search=${name}`)

        return response.length > 0;

    }

    async existByEmail(email: string | undefined): Promise<boolean> {

        const response: Array<UserResponse> = await this.http.get(`${this.baseURL}/users?email=${email}`)

        return response.length > 0;

    }

    async existByName(name: string): Promise<boolean> {
        const response = await this.http.get(`${this.baseURL}/users?username=${name}`);
        return response.length > 0;
    }

    async register(name: string, email: string | undefined, password: string, group: string): Promise<void> {

        await this.http.post
        (
            `${this.baseURL}/users`,
            {
                "username": name,
                "email": email,
                "emailVerified": true,
                "credentials": [
                    {
                        "value": password,
                        "type": "password"
                    }
                ],
                "groups": [group],
                enabled: true
            }
        )

    }

    async registerInGroup(name: string, groupName: string, groupId: string): Promise<void> {

        const user: Array<UserResponse> = await this.http.get(`${this.baseURL}/users?username=${name}`);

        let userResponse: UserResponse | undefined = user.pop();

        if (userResponse !== undefined) {

            await this.http.put
            (
                `${this.baseURL}/users/${userResponse.id}/groups/${groupId}`
            )

        }

    }

}