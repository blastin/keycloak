export interface UserResponse {
    username: string;
    email?: string;
    groups: [string];

    id: string;

}