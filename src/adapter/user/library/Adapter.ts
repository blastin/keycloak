import KcAdminClient, {requiredAction} from '@keycloak/keycloak-admin-client';
import {UserGateway} from '../../../domain/user/Gateway';

interface UserInfo {
    id: string;
    groups: string[];

}

export class Adapter implements UserGateway {

    constructor(private admin: KcAdminClient) {
        this.cache = new Map<string, UserInfo>();
    }

    private cache: Map<string, UserInfo>;

    async belongsToTheGroup(name: string, group: string): Promise<boolean> {

        if (this.cache.get(name)?.groups.some(element => element == group)) {

            console.log(`Usuário '${name}' já está cadastrado no grupo '${group}'`)

            return true;

        }

        console.log(`Usuário '${name}' não está cadastrado no grupo '${group}' `);

        return false;

    }

    async existByName(name: string): Promise<boolean> {

        if (this.cache.get(name) != undefined) {
            console.log(`Usuário '${name}' foi encontrado. Recuperando em cache`);
            return true;
        }

        const promise = await this.admin.users.find({username: name});

        const user = promise.pop();

        if (user != undefined) {

            const groups = await this.admin.users.listGroups({id: user.id || ""});

            this.cache.set
            (
                name,
                {
                    id: user.id || "",
                    groups: groups.map(group => group.name || ""),
                }
            );

            console.log(`Usuário '${name}:${user.email}' foi encontrado. Guardado em cache`);

        } else console.log(`Usuário '${name}' não foi encontrado`);

        return user != undefined;

    }

    async register(name: string, email: string | undefined, password: string, group: string): Promise<void> {

        const promise = await this.admin.users.create
        (
            {
                username: name,
                email: email,
                emailVerified: email != undefined,
                requiredActions: [requiredAction.UPDATE_PASSWORD],
                credentials: [{
                    type: "password",
                    value: password,
                    temporary: true
                }],
                enabled: true,
                groups: [group]
            }
        );

        console.log(`Guardando Usuário '${name}:${promise.id}' em cache`);

        this.cache.set(name, {
            id: promise.id || "",
            groups: [group]
        });

    }

    async registerInGroup(name: string, groupName: string, groupId: string): Promise<void> {

        await this.admin.users.addToGroup
        (
            {
                groupId: groupId,
                id: this.cache.get(name)?.id || ""
            }
        );

        console.log(`Guardando grupo '${groupName}' em cache do usuário ${name} `);

        this.cache.get(name)?.groups.push(groupName);

    }

    async existByEmail(email: string | undefined): Promise<boolean> {

        if (email == undefined) return false;

        const promise = await this.admin.users.find({email: email});

        let user = promise.pop();

        if (user != undefined) console.error(`Usuário ${user.username} com email '${user.email}' cadastrado`);

        else console.log(`não foi encontrado um usuario com email '${email}'`);

        return user != undefined;

    }

}