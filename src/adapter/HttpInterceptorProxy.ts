import {Http} from "./Http";
import fetch from 'node-fetch';
import {NodeFetchHttp} from "./NodeFetchHttp.js";

interface Response {
    access_token: string;
    expires_in: number;
}

export class HttpInterceptorProxy implements Http, Token {
    constructor(urlToken: string, credentials: any, proxy?: any) {
        this.http = new NodeFetchHttp(this, proxy);
        this.urlToken = urlToken;
        this.credentials = credentials;
        this.tokenValue = undefined;
        this.time = 0;
        this.proxy = proxy;
    }

    private readonly proxy: any;

    private readonly http: Http;

    private readonly urlToken: string;

    private readonly credentials: any;

    private time: number;

    private tokenValue?: string;

    async get(url: string): Promise<any> {
        return this.http.get(url);
    }

    post(url: string, body: any): Promise<void> {
        return this.http.post(url, body);
    }

    put(url: string, body?: any): Promise<void> {
        return this.http.put(url, body);
    }

    async value(): Promise<string> {

        if (await this.expired() || this.tokenValue === undefined) {
            return await this.processToken();
        }

        return this.tokenValue;

    }

    private async processToken() {

        console.log(`Recuperando um novo token : ${this.urlToken}`);

        const response = await fetch
        (
            this.urlToken,
            {
                agent: this.proxy,
                method: 'POST',
                body: new URLSearchParams(this.credentials),
                headers: {
                    'content-type': 'application/x-www-form-urlencoded'
                }
            }
        );

        const json: Response = JSON.parse(await response.text());

        this.time = Date.now() + (json.expires_in - 5) * 1000;

        console.log(`Token Status : ${response.status}\tExpires In: ${json.expires_in}\tNow: ${Date.now()}\tToken Expires In: ${this.time}`);

        this.tokenValue = json.access_token;

        return this.tokenValue;

    }

    private async expired() {
        const now = Date.now();
        const bool = now > this.time;
        if (bool) console.log(`Now : ${new Date(now)}\tDate: ${new Date(this.time)}`);
        return bool;
    }

}