import {Group, GroupType} from "../domain/group/Group.js";

import {User, UserType} from "../domain/user/User.js";

export interface Input {

    group: string;

    users: Array<UserInput>;

}

export interface UserInput {

    name: string;

    email?: string;

    password?: string;

}

function group(input: Input): Group {

    return new Group
    (
        input.group,

        input.users.map
        (
            user => new User(user.name, user.email, user.password)
        )
    );

}

function userInput(userType: UserType): UserType {
    return {
        email: userType.email,
        name: userType.name,
        password: userType.password
    }
}

export function input(groupType: GroupType): Input {
    return {
        group: groupType.name,
        users: groupType.users.map(user => userInput(user))
    };
}

export default function (inputs: Array<Input>): Array<Group> {
    return inputs.map(input => group(input));
}

