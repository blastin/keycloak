interface Token {

    value(): Promise<string>;

}