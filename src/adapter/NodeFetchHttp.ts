import {Http} from "./Http";
import fetch from 'node-fetch';

export class NodeFetchHttp implements Http {
    constructor(token: Token, proxy?: any) {
        this.proxy = proxy;
        this.token = token;
    }

    private readonly token: Token;

    private readonly proxy?: any;

    async get(url: string): Promise<any> {

        console.log(`Call:\n\tMethod: [GET]\n\turl: [${url}]\n`);

        return await fetch
        (
            url,
            {
                agent: this.proxy,
                method: 'GET',
                headers: {
                    "Authorization": `Bearer ${await this.token.value()}`
                }
            }
        ).then(async response => {

            if (response.status >= 200 && response.status < 300) {
                return response.json();
            }

            throw new Error(await response.text());

        });

    }

    async post(url: string, body: any): Promise<void> {
        await this.transaction(url, 'POST', body);
    }

    async put(url: string, body: any): Promise<void> {
        await this.transaction(url, 'PUT', body);
    }

    private async transaction(url: string, method: string, body?: any) {

        console.log(`Call:\n\tMethod: [${method}]\n\turl: [${url}]\n\tbody:${JSON.stringify(body)}\n`);

        await fetch
        (
            url,
            {
                agent: this.proxy,
                method: method,
                headers: {
                    "Authorization": `Bearer ${await this.token.value()}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body)
            }
        ).then(async response => {

            if (!(response.status >= 200 && response.status < 300))
                throw new Error(await response.text());

        });


    }

}