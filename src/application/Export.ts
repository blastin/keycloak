import {Input, input} from "../adapter/EntryObject.js";
import {GroupGateway} from "../domain/group/Gateway";

export class Export {

    constructor(private groupGateway: GroupGateway) {
    }

    public async retrieveAll(): Promise<Array<Input>> {

        return this
            .groupGateway
            .all()
            .then
            (
                groups => groups.flatMap(group => input(group.toType()))
            );

    }

}