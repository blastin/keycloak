import {GroupGateway} from "../domain/group/Gateway";
import {Group} from "../domain/group/Group";
import {UserGateway} from "../domain/user/Gateway";

export class Import {

    constructor(private userGateway: UserGateway, private groupGateway: GroupGateway) {
    }

    public async usersGroups(groups: Array<Group>) {
        for (const group of groups) {
            await group.register(this.groupGateway, this.userGateway);
        }
    }

}