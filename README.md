# Projeto Automação do Keycloak

## Instruções de ambiente

Crie um arquivo *.env* na raiz do projeto. Seguindo o exemplo abaixo

```env
USERNAME_KEYCLOAK="user"
PASSWORD_KEYCLOAK="password"
BASE_URL_KEYCLOAK="https://keycloak.com.br"
REALM_NAME_KEYCLOAK="master"
TOKEN_URL_KEYCLOAK="realms/master/protocol/openid-connect/token"
CLIENT_ID_KEYCLOAK="id"
CLIENT_SECRET_KEYCLOAK="secret"
PROXY_HOST="localhost"
PROXY_PORT=3128
```

## Instruções para instalar dependências do projeto

```sh
npm install
```

## Instruções para rodar o projeto

### Importação

* Caso queira realizar uma importação de novos usuários com seus respectivos grupos, crie um arquivo de entrada conforme
  o exemplo abaixo:

```json
[
  {
    "group": "futebol",
    "users": [
      {
        "name": "atacante",
        "email": "atacante@email.com.br",
        "password": "123"
      },
      {
        "name": "defensor"
      }
    ]
  }
]
```

* Rode o seguinte comando

```sh
npm start -type import -file input.json
```

### Exportação

* Caso queira exportar todos usuários com seus respectivos grupos, execute a seguinte linha de comando

```sh
npm start -type export -output /tmp
```

### Argumento de entrada

| Argumento | Descrição          |
|-----------|--------------------|
| -type     | Import ou Export   |
| -file     | Arquivo de entrada |
| -output   | Pasta de saida     |

